<?php
/**
 * Configuration class
 **/
class Config {
    public static $debug = true;

    public static  $dbInfo = array(
        'driver' => 'mysql:host=localhost;dbname=database',
        'username' => 'root',
        'password' => ''
    );

    public static $app = array(
        'hostname' => 'localhost',
        'baseurl' => '',
    );

    public static $path = array(
        'views' => 'app/view/',
        'controller' => 'app/controller/',
        'model' => 'app/model/'
    );
}