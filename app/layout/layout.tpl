<!doctype html>
<html>
<head>
    <title>PAWS</title>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" type="text/css" href="assets/css/style.css"/>
    <link rel="stylesheet" type="text/css" href="assets/css/dev.css"/>
</head>
<body>
    ?$layout.notification
    <div class="notification {$layout.notification.type}">
      ?$layout.notification.title
        <strong>{$layout.notification.title}</strong><br />
        {$layout.notification.message}
      $layout.notification.title?
    </div>
    $layout.notification?

    <section id="content">
    (( content ))
    </section>
</body>
</html>