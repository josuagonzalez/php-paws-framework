<?php
class Layout {
    public static function setHeader() {
        header('Content-Type: text/html; charset=utf-8');
    }

    public static function prepareLayout() {
        $data = array();

        // Put the stuff you want to put into the layout template vars here

        // if there is a notfication
        if(isset($_SESSION['n.message'])) {
            $data['notification'] = array('message' => $_SESSION['n.message'], 'title' => $_SESSION['n.title'], 'type' => $_SESSION['n.type']);
            Helpers::unsetNotification();
        } else $data['notification'] = false;

        return $data;
    }

    public static function registerDivModifiers() {
        div::addCustomModifier('nameMonth:', 'Helpers::nameMonth');
        div::addCustomModifier('nameDay:', 'Helpers::nameDay');
    }
}