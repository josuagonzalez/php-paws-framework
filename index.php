<?php
// First of all : we initialize the session and set charset
session_start();
header('Content-Type: text/html; charset=utf-8');

// Load external libs
require_once('app/lib/div/div.php');

// Load system classes
$classes = array('Config', 'Model', 'Controller', 'Helpers', 'Route', 'Layout', 'Paws');
foreach ($classes as $class) {
    require_once 'app/'.$class.'.class.php';
}

// Instantiate the website
Paws::getInstance()->router($_SERVER['QUERY_STRING']);